/*Write a program that reads the radius of a sphere and prints its volume and
surface area. Use the following formulas. Print the output to four decimal places.
Volume = (4/3)*pi*r^3   Surface Area = 4*pi*r^2
r represents the radius.*/

package project2;

import java.util.Scanner;
import java.text.DecimalFormat;

public class PP3_6 {

	public static void main(String[] args)
	{
		int radius;
		double volume, surfArea;
		
		Scanner scan = new Scanner(System.in);
		//Round output to 4 decimal places
		DecimalFormat fmt = new DecimalFormat("0.####");
		
		System.out.print("Enter the radius of your sphere: ");
		radius = scan.nextInt();
		
		volume = (4.0 / 3.0) * (Math.PI) * Math.pow(radius, 3);
		surfArea = 4 * Math.PI * Math.pow(radius,  2);
		
		//System.out.println("The volume of the sphere is: " + volume);
		//System.out.println("The area of the sphere is: " + surfArea);
		System.out.println("\nThe volume of the sphere is: " + fmt.format(volume));
		System.out.println("The area of the sphere is: " + fmt.format(surfArea));

		scan.close();
	}
}
