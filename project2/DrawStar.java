package project2;

import javax.swing.*;

public class DrawStar {

	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Star Draw");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().add(new StarPanel());

		frame.pack();
		frame.setVisible(true);
	}
}
