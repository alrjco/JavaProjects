/*Write a class called Flight that represents an airline flight. It should contain
instance data that represents the airline name, flight number, and the flight�s
origin and destination cities. Define the Flight constructor to accept and initialize
all instance data. Include getter and setter methods for all instance data. Include
a toString method that returns a one-line description of the flight. Create a driver
class called FlightTest, whose main method instantiates and updates several
Flight objects.*/

package project2;

public class Flight {

	private String name, orgCity, destCity;
	private int fltNum;
	
	//Constructor for Flight objects
	public Flight(String airline, String origin, String destination, int flight)
	{
		name = airline;
		orgCity = origin;
		destCity = destination;
		fltNum = flight;
	}
	
	
	//Getters
	public String getName()
	{
		return name;
	}
	public String getOrgCity()
	{
		return orgCity;
	}
	public String getDestCity()
	{
		return destCity;
	}
	public int getFltNum()
	{
		return fltNum;
	}
	
	
	//Setters
	public void setName(String airline)
	{
		name = airline;
	}
	
	public void setOrgCity(String origin)
	{
		orgCity = origin;
	}
	public void setDestCity(String destination)
	{
		destCity = destination;
	}
	public void setFltNum(int flight)
	{
		fltNum = flight;
	}
	
	
	//To string
	public String toString()
	{
		return (name + " " + fltNum + "\nFrom: " + orgCity + "\nTo: " + destCity);
	}
}