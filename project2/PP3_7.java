/*Write a program that reads the lengths of the sides of a triangle from the user.
Compute the area of the triangle using Heron�s formula (below), in which s
represents half of the perimeter of the triangle and a, b, and c represent 
the lengths of the three sides. Print the area to three decimal places.
Area = sqrt((s(s-a)(s-b)(s-c))
*/

package project2;

import java.util.Scanner;
import java.text.DecimalFormat;


public class PP3_7 {

	public static void main(String[] args)
	{
		double a, b, c, s, area;
		
		Scanner scan = new Scanner(System.in);
		DecimalFormat fmt = new DecimalFormat("0.###");
		
		System.out.print("Enter the three sides of the triangle: \na = ");
		a = scan.nextDouble();
		System.out.print("b = ");
		b = scan.nextDouble();
		System.out.print("c = ");
		c = scan.nextDouble();
		
		s = (a + b + c) / 2;
		
		area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
		
		System.out.print("The area of the triangle is: " + fmt.format(area));
		
		scan.close();
	}
}
