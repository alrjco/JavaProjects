/*Write a program that prompts for and reads the user�s first and last name
(separately). Then print a string composed of the first letter of the user�s first
name, followed by the first five characters of the user�s last name, followed by a
random number in the range 10 to 99. Assume that the last name is at least five
letters long. Similar algorithms are sometimes used to generate usernames for
new computer accounts.*/

package project2;

import java.util.Scanner;
import java.util.Random;

public class PP3_1 {
	
	public static void main(String[] args)
	{
		//final int MIN = 10;
		//final int MAX = 99;	
		
		String nameLast, nameFirst, userFirst, userLast;
		int userInt;
		
		Scanner scan = new Scanner(System.in);
		Random generator = new Random();
		
		//Generate random integer that will be appended to username
		userInt = generator.nextInt(99);
		
		//Get first name
		System.out.print("Please enter your first name: ");
		nameFirst = scan.nextLine();
		userFirst = nameFirst.substring(0, 1);
		
		
		//Get last name
		System.out.print("Please enter your last name: ");
		nameLast = scan.nextLine();
		userLast = nameLast.substring(0, 5);
		
		//Print name and username
		System.out.println("\nYour name is: " + nameFirst + " " + nameLast);
		System.out.println("Your username is: " + userFirst + userLast + userInt);
		
		scan.close();
	}
	
	
}
