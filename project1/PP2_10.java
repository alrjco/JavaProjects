/*
Write a program that determines the value of the coins in a jar and
prints the total in dollars and cents. Read integer values that represent
the number of quarters, dimes, nickels, and pennies.
*/

package project1;

import java.util.Scanner;
import java.text.NumberFormat;

public class PP2_10 
{

	
	public static void main(String[] args)
	{
		double dollars, quarters, dimes, nickels, pennies;
		
		Scanner scan = new Scanner(System.in);
		
		NumberFormat fmt1 = NumberFormat.getCurrencyInstance();
		
		System.out.print("Enter number of quarters in the jar: ");
		quarters = scan.nextDouble();
		
		System.out.print("Enter number of dimes in the jar: ");
		dimes = scan.nextDouble();
		
		System.out.print("Enter number of nickels in the jar: ");
		nickels = scan.nextDouble();
		
		System.out.print("Enter number of pennies in the jar: ");
		pennies = scan.nextDouble();
		
		scan.close();
		
		dollars = (quarters * .25) + (dimes * .10) + (nickels * .05) + (pennies * .01);
		
		System.out.println("There is " + fmt1.format(dollars) + " in the jar!");
	}
}
