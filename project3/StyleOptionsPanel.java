package project3;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class StyleOptionsPanel extends JPanel
{
	private JLabel	saying;
	private JCheckBox	bold, italic;
	private JTextField text;
	int size;
	int style = Font.PLAIN;

	public StyleOptionsPanel()
	{
		text = new JTextField(10);
		text.setText("36");
		saying = new JLabel("Say it with style!");
		
		saying.setFont(new Font("Helvetica", style, Integer.parseInt(text.getText())));

		TextListener txtListener = new TextListener();
		text.addActionListener(txtListener);
		
		bold = new JCheckBox("Bold");
		bold.setBackground(Color.cyan);
		italic = new JCheckBox("Italic");
		italic.setBackground(Color.cyan);
		
		StyleListener styleListener = new StyleListener();
		bold.addItemListener(styleListener);
		italic.addItemListener(styleListener);

		add(saying);
		add(bold);
		add(italic);
		add(new JLabel("Enter text size: "));
		add(text);

		setBackground(Color.cyan);
		setPreferredSize(new Dimension(400, 200));
	}
	
	class TextListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if (bold.isSelected())
				style = Font.BOLD;
			
			if (italic.isSelected())
				style += Font.ITALIC;
			
			size = Integer.parseInt(text.getText());
			
			saying.setFont(new Font("Helvetica", style, size));
		}
	}

	private class StyleListener implements ItemListener
	{
		public void itemStateChanged(ItemEvent event)
		{
			if (bold.isSelected())
				style = Font.BOLD;

			if (italic.isSelected())
				style += Font.ITALIC;
			
			size = Integer.parseInt(text.getText()); 
			
			saying.setFont(new Font("Helvetica", style, size));
		}
	}
}
